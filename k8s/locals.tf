locals {

  environment = terraform.workspace

  cluster_name = length(var.cluster_name) != 0 ? var.cluster_name : format("aws-ingenio-%s", local.environment)
}